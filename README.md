# Heroku-Buildpack-Rails-Api

## Purpose

To support staging apps that are triggered by git clones of the Ember-Client repo.

## Heroku App Order of Operations
*	Bitbucket pipeline performs a `git push` of the Ember-Client source code to remote:Heroku
*	SSH buildpack is loaded
*	SSH buildpack is run, saving SSH private key and known hosts file
*	heroku/nodejs buildpack is loaded, making Node and npm available for later operations
*	This buildpack is loaded
*	This buildpack runs: building the Ember-Client, cloning the Rails-Api server (HEAD of branch:master), reordering the code directories in the build directory
*	The Ruby buildpack is loaded
*	The Ruby buildpack runs, installing all gems, then starts the Rails-Api server

## Public repo

This is a public repository, as Heroku does not support accessing private repositories (requiring authentication) to clone buildpacks.
